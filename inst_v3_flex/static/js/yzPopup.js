const $body = document.getElementById('body');

let popupWrapper = document.createElement("div");

popupWrapper.classList.add('popupWrapper', 'ppHide');
popupWrapper.innerHTML = '<div class="popupClose" id="popupClose"></div><div class="popupContent" id="popupContent"></div>';
$body.appendChild(popupWrapper);

const $popupContent = document.getElementById('popupContent');
const $popupClose = document.getElementById('popupClose');

$popupClose.addEventListener('click', closePopup);
// popupWrapper.addEventListener('click', function (e) {
//     if (e.target.classList.contains('popupWrapper')) {
//         closePopup(e);
//     }
// })
document.addEventListener("keyup", function (event) {
    if (event.keyCode === 27) {
        closePopup(event);
    }
});

function popupInitialize(trigger, clone) {

    let popupTargetLink = trigger.getAttribute('popuptarget');
    let popupTargetElement = document.getElementById(popupTargetLink);


    if (clone) {
        popupTargetElement = popupTargetElement.cloneNode(true);
    } else {
        popupTargetElement.classList.add('ppHide');
    }

    trigger.addEventListener('click', function (event) {
        openPopup(event, popupTargetElement);
    });


}

function openPopup(event, content) {

    event.preventDefault();

    toggleClass($body, 'ppOpened', 'add');
    toggleClass(popupWrapper, 'ppHide', 'remove');
    toggleClass(content, 'ppHide', 'remove');

    $popupContent.appendChild(content);

}

function closePopup(event) {

    event.preventDefault();

    toggleClass($body, 'ppOpened', 'remove');
    toggleClass(popupWrapper, 'ppHide', 'add');

    $popupContent.innerHTML = '';

}

function toggleClass(element, name, action) {
    element.classList[action](name);
}
// ppElement ppHide     