let models = document.querySelectorAll('[pep-model]');
let binds = document.querySelectorAll('[pep-bind]');
let $scope = {};

let pepApp = {
    load: pepAppLoad,
    addLogicToProperty: addLogicToProperty
}

function pepAppLoad() {
    models.forEach(function (model) {

        let propName = model.getAttribute('pep-model');
        pepApp.addLogicToProperty(propName);

        model.addEventListener('keyup', function (e) {
            $scope[propName] = model.value;
            console.log($scope);
        })
    })
}

function addLogicToProperty(property) {
    if (!$scope.hasOwnProperty(property)) {

        let value;

        Object.defineProperty($scope, property, {
            set: function (newValue) {

                value = newValue;

                document.querySelectorAll('[pep-model="' + property + '"]').forEach(function (el) {
                    el.value = newValue;
                })

                document.querySelectorAll('[pep-bind="' + property + '"]').forEach(function (el) {
                    el.innerHTML = newValue;
                })

            },
            get: function () {
                return value;
            }
        })
    }
}

pepApp.load();
$scope.testInput1 = 'testInput1';
$scope.testInput2 = 'testInput2';