let Targets = {
    'jsEditUser': function (e) {

        let userId = e.target.closest('.userTab').getAttribute('data-userid');
        openUserEditPage(userId);

    },
    'jsLoginUser': function (e) {
        checkUser();
    },
    'jsLogoutUser': function (e) {
        logoutUser();
    },
    'jsCreateUser': function (e) {
        createUser();
    },
    'jsUpdateUserInfo': function (e) {
        let userId = $editUserForm.getAttribute('data-userid');
        updateUserInfo(userId);
    },
    'jsDiscardUserEditing': function (e) {
        closeUserEditPage();
    },
    'jsDeleteMediaElement': function (e) {
        let mediaElementId = e.target.getAttribute('data-media-elementid');
        DB.deleteMediaElement(mediaElementId).then(function(data){
            renderUserMedia();
            handleNotification(data.statusText, data.status);
        });
    },
    'jsReplyComment': function (e) {
        commentReply(e.target);
    }
}

document.addEventListener('click', function (e) {
    let targetJsClass = e.target.getAttribute('data-inst-action');

    if (targetJsClass) {
        e.preventDefault();
        Targets[targetJsClass](e);
    }
})

function preventFormSubmit(form) {
    form.addEventListener('submit', function (e) {
        e.preventDefault();
    })
}

window.onhashchange = function () {
    App.load();
}
