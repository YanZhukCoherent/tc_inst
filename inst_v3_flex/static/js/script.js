const $authFormsWrapper = document.querySelector('.instAuthForm');

const $authFormTogglers = document.querySelectorAll('.instAuthForm .message a');

const $loginForm = document.getElementById('loginForm');
const $registerForm = document.getElementById('registerForm');
const $editUserForm = document.getElementById('editUserForm');

const $createUserBtn = document.getElementById('createUserBtn');
const $logoutUserBtn = document.getElementById('logoutUserBtn');
const $editUserBtn = document.getElementById('editUserBtn');

const $mainMenu = document.getElementById('mainMenu');

const $userInfo = document.getElementById('userInfo');
const $notificationHandler = document.getElementById('notificationHandler');
const $mainPage = document.getElementById('mainPage');
const $instFeed = document.getElementById('instFeed');
const $userList = document.getElementById('userList');
const $editUserPage = document.getElementById('editUserPage');
const $uiKitPage = document.getElementById('uiKit');
const $liveChanges = document.getElementById('liveChanges');
const $userMedia = document.getElementById('userMedia');

//upload inputs
const $uploadUserAvatar = document.getElementById('uploadAvatarInput');
const $uploadUserMedia = document.getElementById('uploadUserMedia');

const $openAuthFormBtn = document.getElementById('openAuthFormBtn');

const appModules = [$liveChanges, $mainPage, $instFeed, $userList, $editUserPage, $uiKitPage];



let changeHtml = {
    toggleForms: toggleForms,
    elementToggleClassHide: elementToggleClassHide,
    userInfoContent: userInfoContent,
    removeElement: removeElement
}

let App = {
    load: loadApp,
    renderMenu: renderMenu,
    openPage: openPage,
    runPageMethods: runPageMethods,
    loadFeed: loadFeed
}


const appPages = {
    '#Home': {
        'name': 'Home',
        'menuItem': true,
        'modules': [$mainPage]
    },
    '#Feed': {
        'name': 'Feed',
        'menuItem': true,
        'modules': [$instFeed],
        'methods': [App.loadFeed]
    },
    '#UserList': {
        'name': 'UserList',
        'menuItem': true,
        'modules': [$userList]
    },
    '#liveChanges': {
        'name': 'liveChanges',
        'menuItem': true,
        'modules': [$liveChanges]
    },
    '#EditUser': {
        'name': 'EditUser',
        'menuItem': false,
        'modules': [$editUserPage],
        'methods': [initializeEditForm]
    },
    '#UiKit': {
        'name': 'UiKit',
        'menuItem': true,
        'modules': [$uiKitPage]
    }
}
//////////////////////////////////////////////////////////

App.load();
App.renderMenu($mainMenu);

loadUsersList();
document.addEventListener('baseChanged', loadUsersList);

ifLogged();

preventFormSubmit($loginForm);
preventFormSubmit($registerForm);
preventFormSubmit($editUserForm);

$authFormTogglers.forEach(hangEventsOnTogglers);

document.addEventListener("keypress", function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
    }
});


/////////////////////////////////////////////////////////

function parseHash(hash) {
    // let hash1 = hash.substring(1);
    return hash.split('/');
}

function loadApp() {
    let currentPage = location.hash;
    if (!currentPage[0]) {
        currentPage = '#Home'
    };
    App.openPage(currentPage);
}

function renderMenu(menu) {

    for (let page in appPages) {

        if (appPages[page].menuItem) {
            let menuElement = document.createElement("a");
            menuElement.classList.add('menuElement');
            menuElement.setAttribute('href', page);
            menuElement.innerHTML = appPages[page].name;
            menu.appendChild(menuElement);
        }
    }
}

function openPage(hash) {

    location.hash = hash;

    appModules.forEach(function (el) {
        changeHtml.elementToggleClassHide(el, 'add');
    });

    let page = parseHash(hash)[0];
    appPages[page].modules.forEach(function (el) {
        changeHtml.elementToggleClassHide(el, 'remove');
    });

    if (appPages[page].methods) {
        App.runPageMethods(page);
    }

}

function runPageMethods(page) {
    appPages[page].methods.forEach(function (method) {
        method();
    })
}

function toggleForms(forms) {
    forms.forEach(function (form) {
        form.classList.toggle('active');
    });
};

function elementToggleClassHide(element, action) {
    element.classList[action]('hide');
};

function userInfoContent(content) {
    $userInfo.innerHTML = content;
};

function removeElement(parent, element) {
    parent.removeChild(element);
}

function hangEventsOnTogglers(toggler) {
    toggler.addEventListener('click', function (event) {
        event.preventDefault();
        changeHtml.toggleForms([$loginForm, $registerForm]);
    })
}

function loadUsersList() {

    DB.getUsers().then(function (data) {
        $userList.innerHTML = '';
        data.forEach(renderUser);
    });

}

function renderUser(user) {
    let donor = UIKit.get('kitUserTab');
    let donor2 = UIKit.get('kitUserEditBtn');

    let userTab = donor.cloneNode(true);
    let editBtn = donor2.cloneNode(true)

    userTab.setAttribute('data-userid', user.id);
    userTab.getElementsByClassName('userName')[0].innerHTML = '';
    userTab.getElementsByClassName('userMail')[0].innerHTML = '';
    userTab.getElementsByClassName('userDoB')[0].innerHTML = '';

    if (user.avatar) userTab.getElementsByClassName('userAvatar')[0].innerHTML = '<img src="/uploads/avatars/' + user.avatar + '">';
    if (user.name) userTab.getElementsByClassName('userName')[0].innerHTML = user.name;
    if (user.login) userTab.getElementsByClassName('userMail')[0].innerHTML = user.login;

    let loggedUser = $localStorage.loadField('loggedUser');
    if (loggedUser.login == user.login) {

        editBtn.setAttribute('href', '#EditUser/' + user._id);
        userTab.getElementsByClassName('tabBtns')[0].appendChild(editBtn);
    }

    $userList.appendChild(userTab);
}

function renderUserMedia() {

    let userId = parseHash(location.hash)[1];

    DB.getUserMedia(userId).then(function (media) {
        $userMedia.innerHTML = '';
        console.log(media)
        let donor = UIKit.get('kitUserMediaElement');
        donor.getElementsByClassName('content')[0].innerHTML = '';

        media.forEach(function (element) {
            let mediaElement = donor.cloneNode(true);
            mediaElement.getElementsByClassName('content')[0].innerHTML = '<img src="/uploads/media/' + userId + element.elementPath + '" alt="">';
            mediaElement.getElementsByClassName('delete')[0].setAttribute('data-media-elementid', element._id);
            $userMedia.appendChild(mediaElement);
        })
    })

}

function loadFeed() {

    DB.getFeed().then(function (data) {
        renderFeed(data);
    });

}

function renderFeed(feed) {
    $instFeed.innerHTML = '';
    let donor = UIKit.get('kitFeedElement');
    feed.forEach(function (element) {
        let mediaElement = donor.cloneNode(true);

        DB.getUserAvatar(element.userId).then(function (data) {
            if (data.length > 0) {
                mediaElement.getElementsByClassName('userAvatar')[0].innerHTML = '<img src="/uploads/avatars/' + data + '">';
            }
        });

        mediaElement.getElementsByClassName('content')[0].innerHTML = '<img src="/uploads/media/' + element.userId + element.elementPath + '" alt="">';
        mediaElement.getElementsByClassName('elementUploadDate')[0].innerHTML = parceTime(element.elementUploadTime);
        mediaElement.getElementsByClassName('commentForm')[0].setAttribute('data-target-photo-id', element._id)
        mediaElement.getElementsByClassName('commentList')[0].innerHTML = '';


        let commentsHolder = mediaElement.getElementsByClassName('commentList')[0];

        commentFormSubmit(mediaElement.getElementsByClassName('commentForm')[0], element._id, commentsHolder);

        renderMediaElementComments(element._id, commentsHolder);

        $instFeed.appendChild(mediaElement);
    })
}

function commentFormSubmit(form, elementId, commentsHolder) {
    form.addEventListener('submit', function (e) {
        e.preventDefault();
        if (e.target.getElementsByClassName('commentWrite')[0].value.length > 0) {
            DB.sendComment(e).then(function () {
                renderMediaElementComments(elementId, commentsHolder);
            });
            e.target.getElementsByClassName('commentWrite')[0].classList.remove('warning');
            e.target.getElementsByClassName('commentWrite')[0].value = '';
        } else {
            handleNotification('write something!', 'error');
            e.target.getElementsByClassName('commentWrite')[0].classList.add('warning');
        }
    })
}

function renderMediaElementComments(elementId, commentsHolder) {
    commentsHolder.innerHTML = '';
    return DB.getComments(elementId, 'mediael').then(function (data) {

        let donor = UIKit.get('kitComment');
        data.forEach(function (comment) {

            let commentElement = donor.cloneNode(true);

            DB.getUserAvatar(comment.userId).then(function (data) {
                if (data.length > 0) {
                    commentElement.getElementsByClassName('userAvatar')[0].innerHTML = '<img src="/uploads/avatars/' + data + '">';
                }
            });

            if (comment.commentTarget == "mediael") {
                commentElement.getElementsByClassName('commentText')[0].innerHTML = comment.commentText;

                commentElement.getElementsByClassName('reply')[0].setAttribute('data-comment-reply-id', comment._id);


                let commentReplies = data.filter(function (element) {
                    if (element.commentTarget == "comment" && element.commentReplyId == comment._id) {
                        return element;
                    }
                });

                let replyDonor = UIKit.get('kitCommentReply');
                commentReplies.forEach(function (reply) {

                    let replyElement = replyDonor.cloneNode(true);
                    DB.getUserAvatar(reply.userId).then(function (data) {
                        if (data.length > 0) {
                            replyElement.getElementsByClassName('userAvatar')[0].innerHTML = '<img src="/uploads/avatars/' + data + '">';
                        }
                    });
                    
                    replyElement.getElementsByClassName('commentText')[0].innerHTML = reply.commentText;
                    commentElement.getElementsByClassName('commentReplies')[0].appendChild(replyElement);


                })
                commentsHolder.appendChild(commentElement);
            }


        });
    })
}

function commentReply(replyBtn) {
    let commentId = replyBtn.getAttribute('data-comment-reply-id');
    let commentSection = replyBtn.closest('.commentSection');
    let commentForm = commentSection.getElementsByClassName('commentForm')[0];
    let commentInput = commentSection.getElementsByClassName('commentWrite')[0];

    commentForm.setAttribute('data-comment-target', 'comment');
    commentForm.setAttribute('data-target-comment-id', commentId);

    commentInput.focus();


    console.log(commentId);
    console.log(commentSection);
}

function parceTime(milliseconds) {
    milliseconds = Number(milliseconds)
    let date = new Date(milliseconds);

    let month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let minutes = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
        "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
        "21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
        "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
        "41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
        "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"
    ]

    let Time = {
        year: date.getFullYear(),
        month: date.getMonth(),
        day: date.getDate(),
        hour: date.getHours(),
        minute: date.getMinutes(),
    }
    return Time.day + ' ' + month[Time.month] + ' ' + Time.year + ' at ' + Time.hour + ':' + minutes[Time.minute]
}

function ifLogged() {

    let loggedUser = $localStorage.loadField('loggedUser');

    if (loggedUser.login) {
        loginUser(loggedUser, false);
    } else {
        changeHtml.userInfoContent('Not logged');
        changeHtml.elementToggleClassHide($authFormsWrapper, 'remove');
        changeHtml.elementToggleClassHide($openAuthFormBtn, 'remove');
        changeHtml.elementToggleClassHide($logoutUserBtn, 'add');
    }

}

function createRandomId() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

function loginUser(userInfo, writeLocal) {

    if (writeLocal) {
        $localStorage.writeField('setItem', 'loggedUser', userInfo);
    }

    changeHtml.elementToggleClassHide($authFormsWrapper, 'add');
    changeHtml.elementToggleClassHide($openAuthFormBtn, 'add');
    changeHtml.elementToggleClassHide($logoutUserBtn, 'remove');

    changeHtml.userInfoContent('logged: <span>' + userInfo.login + '</span>');
}

function logoutUser() {
    $localStorage.writeField('removeItem', 'loggedUser');

    changeHtml.elementToggleClassHide($authFormsWrapper, 'remove');
    changeHtml.elementToggleClassHide($openAuthFormBtn, 'remove');
    changeHtml.elementToggleClassHide($logoutUserBtn, 'add');

    changeHtml.userInfoContent('Not logged');

    document.dispatchEvent(baseChanged);

    App.openPage('#UserList');
}

function closeUserEditPage() {

    document.getElementById('editUserName').value = '';
    document.getElementById('editUserDoB').value = '';

    App.openPage('#UserList');

}

function initializeEditForm() {

    document.getElementById('editUserName').value = '';
    document.getElementById('editUserDoB').value = '';
    document.querySelectorAll('.userPhotoWrapper .photo')[0].innerHTML = '';


    let userId = parseHash(location.hash)[1];
    if (userId) {
        Upload.bindChange($uploadUserAvatar, 'avatar', userId);
        Upload.bindChange($uploadUserMedia, 'media', userId);

        DB.getUserById(userId).then(function (user) {

            let formHeading = $editUserForm.getElementsByClassName('formHeading')[0];
            formHeading.innerHTML = 'Edit user <span>' + user.login + '</span> info:';

            if (user.avatar) {
                document.querySelectorAll('.userPhotoWrapper .photo')[0].innerHTML = '<img src="/uploads/avatars/' + user.avatar + '">';
            };
            if (user.name) document.getElementById('editUserName').value = user.name;
            if (user.dob) document.getElementById('editUserDoB').value = user.dob;

            flatpickr("#editUserDoB", {
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
                defaultDate: user.dob
            });

            $editUserForm.setAttribute('data-userid', userId);

        });

        renderUserMedia();

    } else {
        handleNotification('choose user first!', 'error');
        closeUserEditPage()
    }
}



function handleNotification(text, status) {

    let notificationTab = document.createElement("div");

    notificationTab.classList.add('bounceIn', 'notificationTab', status);

    notificationTab.innerHTML = '<div class="notificationText">' + text + '</div><div class="notificationTabClose"></div>';
    $notificationHandler.prepend(notificationTab);

    setTimeout(function () {
        notificationTab.animate([{
                opacity: '1'
            },
            {
                opacity: '0'
            }
        ], {
            duration: 5000
        });
    }, 1000);

    setTimeout(function () {
        changeHtml.removeElement($notificationHandler, notificationTab);
    }, 5900);
}


// popups
popupInitialize($openAuthFormBtn);