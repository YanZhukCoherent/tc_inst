const UIKitContainer = document.getElementById('uiKit');

let UIKit = {
    get: getUIElement
}

function getUIElement(id) {
    let element = document.getElementById(id);
    return element.children[0];
}