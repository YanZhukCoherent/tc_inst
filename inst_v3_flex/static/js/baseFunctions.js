let baseChanged = new Event('baseChanged');

let DB = {
    getUsers: getUsers,
    getUserById: getUserById,
    checkUser: checkUser,
    createUser: createUser,
    sendReqest: sendReqest,
    getUserMedia: getUserMedia,
    getUserAvatar: getUserAvatar,
    deleteMediaElement: deleteMediaElement,
    getFeed: getFeed,
    sendComment: sendComment,
    getComments: getComments
}

function sendReqest(httpMethod, url, data) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        let formData = new FormData();
        for (key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = function (state) {
            if (xhr.readyState == XMLHttpRequest.DONE) {

                resolve(xhr.response);

            }
        }

        xhr.open(httpMethod, url);
        xhr.send(formData);

    })
}

function getUsers() {

    return DB.sendReqest("GET", '/user').then(function (data) {
        return JSON.parse(data);
    })

};

function getUserById(userId) {

    let requestUrl = '/user/' + userId;

    return DB.sendReqest("GET", requestUrl).then(function (data) {
        return JSON.parse(data);
    })

};


function updateUserInfo(userId) {

    let newInfo = {};
    newInfo.name = document.getElementById('editUserName').value;
    newInfo.dob = document.getElementById('editUserDoB').value;

    requestUrl = '/user/' + userId;
    DB.sendReqest('POST', requestUrl, newInfo).then(function (data) {
        data = JSON.parse(data);

        handleNotification(data.statusText, data.status)

        closeUserEditPage();
        document.dispatchEvent(baseChanged);
    })

}


function createUser() {

    let createInfo = {};

    createInfo.login = document.getElementById('regEmail').value;
    createInfo.pass = document.getElementById('regPass').value;

    let confirmPass = document.getElementById('regConfirmPass').value;

    if (createInfo.login) {
        if (createInfo.pass === confirmPass) {

            DB.sendReqest('PUT', '/user', createInfo).then(function (data) {

                data = JSON.parse(data);

                if (data.status == 'error') {
                    handleNotification(data.statusText, data.status);
                } else {
                    handleNotification(data.statusText, data.status);
                    document.dispatchEvent(baseChanged);
                    changeHtml.toggleForms([$loginForm, $registerForm]);
                }
            })


        } else {
            handleNotification('password not confrimed!', 'error');
        };
    } else {
        handleNotification('insert login!', 'error');
    };

};

function checkUser() {

    let loginInfo = {};

    loginInfo.login = document.getElementById('loginEmail').value;
    loginInfo.pass = document.getElementById('loginPass').value;

    DB.sendReqest('GET', '/user/' + loginInfo.login + '/' + loginInfo.pass).then(function (data) {

        data = JSON.parse(data);

        if (data.status == 'error') {
            handleNotification(data.statusText, data.status);
        } else {
            loginUser(data.userInfo, true);
            handleNotification(data.statusText, data.status);
            document.dispatchEvent(baseChanged);
            closePopup(event);
        }
    })

};

function getUserMedia(userId) {
    return DB.sendReqest("GET", '/media/' + userId).then(function (data) {
        data = JSON.parse(data);
        return data;
    })
}

function getUserAvatar(userId) {
    return DB.sendReqest("GET", '/avatar/' + userId).then(function (data) {
        data = JSON.parse(data);
        return data;
    })
}

function getFeed() {
    return DB.sendReqest("GET", '/feed').then(function (data) {
        return JSON.parse(data);
    })
}

function deleteMediaElement(mediaElementId) {
    let sendData = {
        mediaElementId: mediaElementId
    }

    return DB.sendReqest("DELETE", '/media', sendData).then(function (data) {
        return data = JSON.parse(data);
    })
}

const $userPhoto = document.querySelectorAll('.userPhotoWrapper .photo');

let Upload = {
    bindChange: bindChange,
    avatar: uploadAvatar,
    media: uploadMedia
}

function bindChange(input, method, userId) {
    input.onchange = function (e) {
        Upload[method](input, userId);
    }
}

function uploadAvatar(input, userId) {

    let sendData = {
        user: userId,
        photo: input.files[0]
    }

    DB.sendReqest('PUT', '/avatar', sendData).then(function (data) {
        data = JSON.parse(data);

        handleNotification(data.statusText, data.status);
        $userPhoto[0].innerHTML = '<img src="/uploads/avatars/' + data.newAvatar + '">';
        document.dispatchEvent(baseChanged);

    });
}

function uploadMedia(input, userId) {

    DB.sendReqest('PUT', '/media/' + userId, input.files).then(function (data) {
        data = JSON.parse(data);

        renderUserMedia();
        handleNotification(data.statusText, data.status);

    });
}

function sendComment(e) {

    let form = e.target;
    let formTextareaVal = form.getElementsByClassName('commentWrite')[0].value;
    let user = $localStorage.loadField('loggedUser');

    let sendData = {
        commentTarget: form.getAttribute('data-comment-target'),
        commentPhotoId: form.getAttribute('data-target-photo-id'),
        commentReplyId: form.getAttribute('data-target-comment-id'),
        commentText: formTextareaVal,
        userId: user.id
    }
    if (user) {
        return DB.sendReqest('PUT', '/comment', sendData).then(function (data) {
            data = JSON.parse(data);
            handleNotification(data.statusText, data.status);
            return data;
        });
    } else {
        handleNotification('login first', 'error');
    }

}

function getComments(elementId, commentTarget) {

    return DB.sendReqest('GET', '/comment/' + elementId).then(function (data) {
        data = JSON.parse(data);
        return data;
    });

}