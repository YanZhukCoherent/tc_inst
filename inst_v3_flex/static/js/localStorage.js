let $localStorage = {
    loadField: loadField,
    writeField: writeField
}

function loadField(field) {

    let response = JSON.parse(localStorage.getItem(field));

    if (!response) {
        response = [];
    }

    return response;
}


function writeField(action, field, value) {

    value = JSON.stringify(value);

    localStorage[action](field, value);


};