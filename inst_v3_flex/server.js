const path = require('path');

const express = require('express');

const formidable = require('formidable');
const sharp = require('sharp');
const mongoose = require('mongoose');
const fs = require('fs');

const port = 8080;

const app = express();


////////////////////////////////////EXPRESS APP//////////////////////////////////////////////////////////////////////////////


app.use(express.static('static'));

app.listen(port, function () {
    console.log("Server is running on " + port + " port");
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/static/index.html');
});


////////////////////////////////////MONGO////////////////////////////////////////////////////////////////////////////////////////


mongoose.connect('mongodb://localhost:/DB', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let DB = mongoose.connection;

DB.on('error', console.error.bind(console, 'connection error:'));

DB.once('open', function () {
    console.log("we're connected to DB!");

    let UserSchema = new mongoose.Schema({
        login: String,
        pass: String,
        name: String,
        dob: String,
        avatar: String
    });

    let MediaElementSchema = new mongoose.Schema({
        elementPath: String,
        elementUploadTime: String,
        userId: String
    });

    let CommentSchema = new mongoose.Schema({
        commentTarget: String,
        commentPhotoId: String,
        commentReplyId: String,
        commentText: String,
        userId: String
    });

    UserSchema.methods.getId = function () {
        var thisId = this._id
        return JSON.stringify(thisId);
    }

    let registeredUsers = mongoose.model('registeredUsers', UserSchema);
    let mediaElements = mongoose.model('mediaElements', MediaElementSchema);
    let usersComments = mongoose.model('usersComments', CommentSchema);

    // DB.db.listCollections().toArray(function (err, names) {
    //     console.log(names); // [{ name: 'dbname.myCollection' }]
    //     module.exports.Collection = names;
    // });

    // DB.db.dropCollection('registeredusers');
    // DB.db.dropCollection('mediaelements');
    // DB.db.dropCollection('userscomments');

    // registeredUsers.find({}, function (err, docs) {
    //     console.log(docs)
    // })
    // mediaElements.find({}, function (err, docs) {
    //     console.log(docs)
    // })
    // usersComments.find({}, function (err, docs) {
    //     console.log(docs)
    // })

    let Users = {
        createOne: createUser,
        getOneById: getUserById,
        updateOneById: updateUser,
        loginOne: loginUser,
        getAll: getAllUsers,
        loadAvatar: loadAvatar
    }
    let Media = {
        uploadAvatar: uploadAvatar,
        uploadUserMedia: uploadUserMedia,
        getUserMedia: getUserMedia,
        deleteElement: deleteElement,
        getAll: getAllMedia
    }
    let Comment = {
        putOne: putComment,
        getMany: getCommentsArray
    }

    app.route('/user/:userId')
        .get(Users.getOneById)
        .post(Users.updateOneById);

    app.route('/user')
        .put(Users.createOne)
        .get(Users.getAll);

    app.route('/user/:userLogin/:userPass')
        .get(Users.loginOne);

    app.route('/feed')
        .get(Media.getAll);

    app.route('/avatar')
        .put(Media.uploadAvatar);

    app.route('/avatar/:userId')
        .get(Users.loadAvatar);

    app.route('/media/:userId')
        .put(Media.uploadUserMedia)
        .get(Media.getUserMedia);

    app.route('/media')
        .delete(Media.deleteElement);

    app.route('/comment')
        .put(Comment.putOne);

    app.route('/comment/:elementId')
        .get(Comment.getMany);


    //get all users
    function getAllUsers(req, res) {
        registeredUsers.find(function (err, docs) {
            if (err) return console.error(err);
            res.json(docs);
        });
    }

    //create user
    function createUser(req, res) {
        let form = new formidable.IncomingForm();

        form.parse(req, function (err, fields) {
            if (err) throw err;

            let newUser = {};
            for (key in fields) {
                newUser[key] = fields[key];
            }

            newUser = new registeredUsers(newUser)

            registeredUsers.find({
                login: fields.login
            }, function (err, docs) {
                if (err) console.log(err);
                if (docs.length > 0) {
                    res.json({
                        status: 'error',
                        statusText: 'those login already exists'
                    });
                } else {
                    newUser.save(function (err, newUser) {
                        if (err) res.json(err);
                        res.json({
                            status: 'success',
                            statusText: 'new user with login ' + newUser.login + ' was successfully created'
                        });
                    });
                }
            });
        });
    }

    //get user by id
    function getUserById(req, res) {

        let userId = req.params.userId;

        registeredUsers.findOne({
            _id: userId
        }, function (err, doc) {
            if (err) console.log(err);
            res.json(doc);
        });

    }

    //update user info
    function updateUser(req, res) {

        let userId = req.params.userId;
        let form = new formidable.IncomingForm();

        form.parse(req, function (err, fields) {
            if (err) throw err;

            // let newInfo = JSON.parse(fields.updateUser);

            registeredUsers.findOne({
                _id: userId
            }, function (err, doc) {
                for (key in fields) {
                    doc[key] = fields[key];
                }

                doc.save();
                res.json({
                    status: 'success',
                    statusText: 'user info successfully updated'
                })
            });

        });
    }

    //login users
    function loginUser(req, res) {
        let loginInfo = req.params;
        console.log(loginInfo);

        registeredUsers.findOne({
            login: loginInfo.userLogin
        }, function (err, doc) {
            if (err) console.log(err);

            if (!doc) {
                res.json({
                    status: 'error',
                    statusText: 'there is no such user, please register'
                });
            } else {
                if (doc.pass == loginInfo.userPass) {
                    res.json({
                        userInfo: {
                            login: doc.login,
                            id: doc._id
                        },
                        status: 'success',
                        statusText: "logged as " + doc.login
                    })
                } else {
                    res.json({
                        status: 'error',
                        statusText: 'password is incorrect'
                    })
                }
            }
        });
    }


    //upload photo
    function uploadAvatar(req, res) {
        let form = new formidable.IncomingForm();

        form.parse(req, function (err, fields, files) {
            let newName = fields.user + '_avatar.' + getExtension(files.photo.name);

            let oldpath = files.photo.path;
            let newpath = __dirname + '/static/uploads/avatars/' + newName;

            registeredUsers.findOne({
                _id: fields.user
            }, function (err, doc) {
                doc.avatar = newName;
                doc.save();
            });

            sharp(oldpath)
                .resize(200, 200)
                .toFile(newpath, function (err) {
                    if (err) {
                        throw err;
                    } else {
                        res.json({
                            status: 'success',
                            statusText: 'avatar successfully changed',
                            newAvatar: newName
                        });
                    }
                });

        });
    };

    function uploadUserMedia(req, res) {

        let userId = req.params.userId;
        let userDir = './static/uploads/media/' + userId;

        if (!fs.existsSync(userDir)) {
            fs.mkdirSync(userDir);
        }

        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            let i = 0;
            for (key in files) {

                let oldpath = files[key].path;
                let currentTime = new Date;
                let newName = '/photo_' + key + '_' + createRandomId() + '_' + currentTime.getTime() + '.png'
                let newpath = userDir + newName;

                newMediaElement = new mediaElements({
                    elementPath: newName,
                    elementUploadTime: currentTime.getTime(),
                    userId: userId
                });
                newMediaElement.save();

                sharp(oldpath)
                    .resize(700, 700)
                    .png()
                    .toFile(newpath, function (err) {
                        if (err) {
                            throw err;
                        } else {
                            if (i++ === fields.length - 1) {
                                res.json({
                                    status: 'success',
                                    statusText: 'photos uploaded'
                                })
                            }
                        }
                    });
            }
        })
    };

    function getUserMedia(req, res) {

        let userId = req.params.userId;

        mediaElements.find({
            userId: userId
        }, function (err, docs) {
            if (err) throw err;
            res.json(docs)
        });

    };

    function loadAvatar(req, res) {

        let userId = req.params.userId;

        registeredUsers.findOne({
            _id: userId
        }, function (err, doc) {
            if (err) throw err;
            if (doc.avatar) {
                res.json(doc.avatar)
            } else {
                res.json([])
            }

        });

    };

    function getAllMedia(req, res) {
        mediaElements.find()
            .sort({
                elementUploadTime: -1
            })
            .exec(function (err, docs) {
                if (err) throw err;

                res.json(docs)
            });
    }

    function deleteElement(req, res) {


        let form = new formidable.IncomingForm();

        form.parse(req, function (err, fields, files) {
            let mediaElementId = fields.mediaElementId;
            mediaElements.findOne({
                _id: mediaElementId
            }, function (err, doc) {
                if (err) throw err;

                let elementPath = './static/uploads/media/' + doc.userId + doc.elementPath;
                fs.unlink(elementPath, function (err) {
                    if (err) console.error(err);
                    mediaElements.deleteOne(doc, function (err) {
                        if (err) throw err;
                        res.json({
                            status: 'success',
                            statusText: 'file removed'
                        })
                    })
                })
            });

        })

    };



    function putComment(req, res) {
        let form = new formidable.IncomingForm();
        form.parse(req, function (err, fields) {
            if (err) throw err;

            let newComment = new usersComments(fields);
            newComment.save(function (err) {
                if (err) throw err;

                res.json({
                    status: 'success',
                    statusText: 'comment posted'
                });
            });
        });

    }

    function getCommentsArray(req, res) {

        let elementId = req.params.elementId;

        let comentsArray = []
        usersComments.find({
            commentPhotoId: elementId,
        }, function (err, photoComments) {
            if (err) throw err;
            
            res.json(photoComments);
        })

    }






}); //mongoose closes

///////////////////////////////////////////FUNCTIONS/////////////////////////////////////////////////////////////////////////////////


function getExtension(filename) {
    var ext = path.extname(filename || '').split('.');
    return ext[ext.length - 1];
}

function createRandomId() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}